import * as sequelize from "sequelize";


export const dbConfig = new sequelize.Sequelize(
    "demo",
    "postgres",
    "1604",
    {
        port: Number(process.env.DB_PORT) || 5432,
        host: process.env.DB_HOST || "localhost",
        dialect: "postgres",
        pool: {
            min: 0,
            max: 5,
            acquire: 30000,
            idle: 10000,
        },
    }
);

dbConfig.authenticate();
