import express, { Application } from 'express';
import morgan from 'morgan';
// Routes
import userRoutes from './modules/user/router';

export class App {

    private app: Application;

    constructor(private port?: number | string) {
        this.app = express();
        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.app.use(morgan('dev'));
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(express.json());
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
            res.header('Access-Control-Allow-Headers', 'Content-Type');
            next();
        });
    }

    routes() {
        this.app.use('/user', userRoutes);
    }

    async listen() {
        await this.app.listen(8087);
    }

}