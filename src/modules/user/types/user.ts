import { Sequelize, Model, DataTypes, Optional } from "sequelize";
import { dbConfig } from "../../../db/sequelize";
import crypto from 'crypto';
import { v4 as uuidv4 } from 'uuid';
export interface UserRequest {
  amount: number;
  page: number;
  search?: string;
}
export interface UserInstance extends Model {
  id: number;
  name: string;
  email: string;
  password: string;
  phone?: string;
}

export interface IdUserInstance extends Model {
  id: number;
}
const phoneValidationRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im

export const User = dbConfig.define<UserInstance>(
  "users",
  {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      defaultValue: () => uuidv4(),
    },
    name: {
      type:DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate:{
        is : /^[a-zA-Z0-9\._]{4,32}$/
      }
    },
    email: {
      type:DataTypes.STRING,
      allowNull: false,
      validate:{
        isEmail : true
      }
    },
    password:{
      type:DataTypes.STRING,
      allowNull: false,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        validator: function(phone:string) {
          return /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/.test(phone);
          },
      }
    }
  },
  {
    timestamps: false,
  }
);
