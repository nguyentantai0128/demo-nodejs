import { Router } from 'express';
import { list,detail,create,update,deletes,login } from './controller/user.controller';
import {checkDuplicateUsernameOrEmail} from './../../middleware/verifySignUp';
import {auth} from './../../middleware/jwt';

const router = Router();

router.post('/api/create',auth,checkDuplicateUsernameOrEmail,create);
router.post('/api/list',auth,list);
router.post('/api/detail',auth,detail);
router.post('/api/update',auth,update);
router.delete('/api/delete',auth,deletes);
router.post("/login", login);
router.post("/fresh", login);

export default router;  