import { Request, Response } from "express";
import { UserRequest, UserInstance, IdUserInstance } from "../types/user";
import { getListUser, getDetailUser, createUser,updateUser,deleteUser,loginUser,findUser } from "../models/user";

export async function list(req: Request, res: Response) {
    try {
        let params: UserRequest = req.body;
        let results = await getListUser(params);
        return res.status(200).send({ status: 200, message: "Success!", data: results.rows, total:results.count });
    }catch(error) {
        return res.status(500).send({ status: 500, message: error.message, data: false });
    }
}

export async function detail(req: Request, res: Response) {
    try {
        let params: IdUserInstance = req.body;
        let result = await getDetailUser(params);
        if (!result){
            return res.status(500).send({ status: 401, message: "Error !", data: false });
        }
        return res.status(200).send({ status: 200, message: "Success!", data: result });
    } catch(error) {
        return res.status(500).send({ status: 500, message: error.message, data: false });
    }
}

export async function create(req: Request, res: Response) {
    try {
        let params: UserInstance = req.body;
        let user;
        if (params.phone && params.phone.length > 0) {
            user = await findUser(params.phone,'phone');
        }
        if(user != null) {
            return res.status(500).send({ status: 401, message: "Failed! Phone is already in use!", data: false });
        } 
        let result = await createUser(params);
        if (!result){
            return res.status(500).send({ status: 401, message: "Error !", data: false });
        }
        return res.status(200).send({ status: 200, message: "Success!", data: result });
    } catch (error) {
        return res.status(500).send({ status: 500, message: error.message, data: false });
    }
}

export async function update(req: Request, res: Response) {
    try {
        let params: UserInstance = req.body;
        let result = await updateUser(params);
        if (!result){
            return res.status(500).send({ status: 401, message: "Error !", data: false });
        }
        return res.status(200).send({ status: 200, message: "Success!", data: result });
    } catch (error) {
        return res.status(500).send({ status: 500, message: error.message, data: false });
    }
}

export async function deletes(req: Request, res: Response) {
    try {
        let params: IdUserInstance = req.body;
        let result = await deleteUser(params);
        if (!result){
            return res.status(500).send({ status: 401, message: "Error !", data: false });
        }
        return res.status(200).send({ status: 200, message: "Success!", data: result });
    } catch (error) {
        return res.status(500).send({ status: 500, message: error.message, data: false });
    }
}

export async function login(req: Request, res: Response) {
    try {
        let params: UserInstance = req.body;
        if ((!params.name && !params.email) || (!req.body.password)) {
            return res.status(401).send({
                message: 'Please provide username/email and password.'
            });
        }
        let result = await loginUser(params);
        if (!result){
            return res.status(500).send({ status: 401, message: "Please provide username/email and password.", data: false });
        }
        return res.status(200).send({ status: 200, message: "Success!", data: result });
    } catch (error) {
        return res.status(500).send({ status: 500, message: error.message, data: false });
    }
}