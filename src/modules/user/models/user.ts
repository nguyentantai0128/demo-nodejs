import { UserRequest, User, UserInstance, IdUserInstance } from '../types/user';
import { Op } from 'sequelize';
import * as bcrypt from 'bcrypt';
import * as jwt from "jsonwebtoken";
import config from "../../../config/config";

export async function findUser(value: string,key:string) {
    try {
     let users = await User.findAll({attributes: ['id','password'], where: {[key]: value} })
      return (users instanceof Array) ? users[0] : null;
    }
    catch (ex) {
        console.log(ex);
        return;
    }
}

export async function loginUser(params:UserInstance) {
    let user;
    if(params.name) {
        user = await findUser(params.name,'name');
    } else {
        user = await findUser(params.email,'email');
    }
    if(user == null || !(user instanceof User)) {
       return false;
    } 
    let comparePass:Boolean = await bcrypt.compare(params.password, user.password.trim());
    if (!comparePass) {
        return false;
    } 
    const token = jwt.sign({ userId: user.id, username: user.name },config.jwtSecret,
        { expiresIn: "1h" }
    );
    return token
}

export async function getListUser(params: UserRequest) {
    let search = {
        name:{
            [Op.like]: '%'+ params.search +'%'
        }
    };
    const { count, rows } = await User.findAndCountAll({
        attributes: ['id', 'email','name','password'],
        limit:params.amount,
        offset:params.amount * (params.page - 1),
        where:search,
    });
    return { count, rows }
}

export async function getDetailUser(params: IdUserInstance) {
    return await User.findByPk(params.id, {
        attributes: ['id', 'email','name','password']
    })
}

export async function createUser(params:UserInstance) {
    let hash = await bcrypt.hash(params.password, 10);
    return await User.create({
    id: params.id,
    name: params.name,
    email: params.email,
    password: hash,
    phone:params.phone
  });
}

export async function updateUser(params:UserInstance) {
    const [ updated ] = await User.update(params, {
        where: { id: params.id }
      });
    return updated
}

export async function deleteUser(params:IdUserInstance) {
    const deleted = await User.destroy({
        where: { id: params.id }
    });
    return deleted
}



