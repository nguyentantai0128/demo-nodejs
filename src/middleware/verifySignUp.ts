
import { Request, Response,NextFunction } from "express";
import { UserInstance,User } from './../modules/user/types/user';

export const checkDuplicateUsernameOrEmail = (req:Request, res:Response, next:NextFunction) => {
    var users : UserInstance = req.body;
    User.findOne({
      where: {
        name: users.name
      }
    }).then(user => {
        if (user) {
          return res.status(400).send({
              message: "Failed! Username is already in use!"
            });
        }
        User.findOne({
            where: {
              email: users.email
            }
          }).then(user => {
            if (user) {
              return res.status(400).send({
                message: "Failed! Email is already in use!"
              });
            }
            next();
        });
    });
};

