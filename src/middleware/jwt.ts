import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";

export const auth = (req: Request, res: Response, next: NextFunction) => {
  let token = <string>req.headers["auth"] || <string>req.headers['x-access-token'] || <string>req.headers['authorization'];
  if (token && token.startsWith('Bearer ')) {
    token = token.slice(7, token.length)
  }
  if (token) {
    let jwtPayload;
    try {
      jwtPayload = <any>jwt.verify(token, config.jwtSecret);
      res.locals.jwtPayload = jwtPayload;
    } catch (error) {
      return res.status(403).send({status:404,message:"Invalid Access",data:null});
    }
    const { userId, username } = jwtPayload;
    console.log(jwtPayload)
    next();
  } else {
    return res.status(403).send({status:404,message:"Invalid Access",data:null});
  }
};
